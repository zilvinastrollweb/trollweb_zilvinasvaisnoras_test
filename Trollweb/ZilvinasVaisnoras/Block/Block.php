<?php

namespace Trollweb\ZilvinasVaisnoras\Block;

class Block extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    public function getCheckDoesParameterValueIsTroll()
    {
        // will return true or false
        return $this->registry->registry('check_parameter_login_troll');
    }
}