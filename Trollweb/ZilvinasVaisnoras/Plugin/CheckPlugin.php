<?php

namespace Trollweb\ZilvinasVaisnoras\Plugin;

class CheckPlugin
{
    protected $registry;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Registry $registry
    )
    {
        $this->request = $request;
        $this->registry = $registry;
    }

    public function beforeDispatch()
    {
        $this->request->getParams();
        if ($this->request->getParam('login') === 'troll') {
            $this->registry->register('check_parameter_login_troll', true);
        } else {
            $this->registry->register('check_parameter_login_troll', false);
        }
    }
}